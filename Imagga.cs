﻿using ConfigHelper;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Imagga_VR
{
    public class Imagga
    {

        public static string ClassifyImage(string configurationFilePath, string contentId)
        {
            if (contentId == null || contentId == "")
            {
                string message = "Image content id is illiegal: " + contentId;
                throw new System.InvalidOperationException(message);
            }
            try
            {
                //Imagga Details
                string apiEndPoint = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiEndPoint");
                string apiKey = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiKey");
                string apiSecret = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiSecret");


                var client = new RestClient();
                client.BaseUrl = new Uri(apiEndPoint);
                client.Authenticator = new HttpBasicAuthenticator(apiKey, apiSecret);

                var request = new RestRequest(Method.GET);
                request.Resource = "v1/tagging?content={content}";
                //request.Resource = "v1/tagging?url=" + Uri.EscapeDataString("http://playground.imagga.com/static/img/example_photo.jpg");
                
                request.AddUrlSegment("content", contentId);

                // Add HTTP Headers
                //request.AddHeader("Content-type", "application/json");
                //request.AddHeader("Accept", "*/*");

                //Console.WriteLine("Request...\n\nFile Path: " + filePath);

                //request.RequestFormat = DataFormat.Json;

                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string result = response.Content;
                    Console.WriteLine(result);
                    return (result);
                }
                else
                {
                    string message = "Something went wrong...\n" + response.Content;
                    throw new System.InvalidOperationException(message);
                }

            }
            catch (Exception error)
            {
                string message = "Something went wrong...\n" + error.ToString();
                throw new System.InvalidOperationException(message);
            }

        }

        public static string GetTagsForImage(string configurationFilePath, string filePath)
        {
            string result = "";
            string contentResult = UploadImage(configurationFilePath, filePath);
            System.Threading.Thread.Sleep(1000);
            var obj = JObject.Parse(contentResult);
            if ((string)obj["status"] == "success")
            {
                result = ClassifyImage(configurationFilePath, (string)obj["uploaded"][0]["id"]);
            }
            else
            {
                string message = "Something went wrong while trying to upload image to imagga...\n" + contentResult;
            }           

            return result;
        }

        public static string UploadImage(string configurationFilePath, string filePath)
        {
            if (filePath == null || filePath == "" || !System.IO.File.Exists(filePath))
            {
                string message = "File path is illiegal: " + filePath;
                throw new System.InvalidOperationException(message);
            }
            try
            {
                //Imagga Details
                string apiEndPoint = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiEndPoint");
                string apiKey = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiKey");
                string apiSecret = Config.GetConfigurationValue(configurationFilePath, "imaggaVisualRecognition", "apiSecret");


                var client = new RestClient();
                client.BaseUrl = new Uri(apiEndPoint);
                client.Authenticator = new HttpBasicAuthenticator(apiKey, apiSecret);

                var request = new RestRequest(Method.POST);
                request.Resource = "v1/content";                

                // Add HTTP Headers
                request.AddHeader("Content-type", "application/x-www-form-urlencoded");
                request.AddFile("image", File.ReadAllBytes(filePath), Path.GetFileName(filePath), "application/octet-stream");


                request.RequestFormat = DataFormat.Json;

                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string result = response.Content;
                    Console.WriteLine(result);
                    return (result);
                }
                else
                {
                    string message = "Something went wrong...\n" + response.Content;
                    throw new System.InvalidOperationException(message);
                }

            }
            catch (Exception error)
            {
                string message = "Something went wrong...\n" + error.ToString();
                throw new System.InvalidOperationException(message);
            }

        }


        /// <summary>
        /// Upload an image file to the API and get content id for processing it using one of the other endpoint methods. Upload an image file to the API and get content id for processing it using one of the other endpoint method.
        /// </summary>
        /// <param name="image">image</param>
        ///// <returns></returns>
        //public static void Upload(byte[] image)
        //{
        //    // create path and map variables
        //    var path = "/content";

        //    // query params
        //    var queryParams = new Dictionary<String, String>();
        //    var headerParams = new Dictionary<String, String>();
        //    var formParams = new Dictionary<String, object>();

        //    // verify required params are set
        //    if (image == null)
        //    {
        //        string message = "Image parameter is illiegal";
        //        throw new System.InvalidOperationException(message);
                
        //    }
        //    if (image != null)
        //    {
        //        formParams.Add("image", ApiInvoker.parameterToString(image));
        //    }
        //    // authentication setting
        //    bool requireAuth = true;

        //    try
        //    {
        //        if (typeof(UploadsResponse) == typeof(byte[]))
        //        {
        //            var response = apiInvoker.invokeBinaryAPI(basePath, path, "GET", queryParams, null, headerParams, formParams, requireAuth);
        //            Debug.Write("Upload response: " + response.ToString());
        //            return ((object)response) as UploadsResponse;
        //        }
        //        else {
        //            var response = apiInvoker.invokeAPI(basePath, path, "POST", queryParams, null, headerParams, formParams, requireAuth);
        //            if (response != null)
        //            {
        //                return (UploadsResponse)ApiInvoker.deserialize(response, typeof(UploadsResponse));
        //            }
        //            else {
        //                return null;
        //            }
        //        }
        //    }
        //    catch (ApiException ex)
        //    {
        //        if (ex.ErrorCode == 404)
        //        {
        //            return null;
        //        }
        //        else {
        //            throw ex;
        //        }
        //    }
        //}


    }
}
